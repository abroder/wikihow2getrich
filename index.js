#!/usr/bin/env node

var request = require('request')
var cheerio = require('cheerio')
var twit = require('twit')

var config = require('./config')

var T = new twit(config)

const generate = function () {
  request('http://www.wikihow.com/Special:Randomizer', (err, res, body) => {
    if (err) 
      return

    const $ = cheerio.load(body)
    const steps = $('.step b')
    const step = steps[0].children[0].data

    const tweet = `Step 1. ${ step }\nStep 2. ???\nStep 3. Profit!`

    if (step === undefined || tweet.length > 140) {
      generate()
    } else {
      T.post('statuses/update', { status: tweet }, (err, data, res) => {
        if (err) {
          console.error('Error posting tweet', err);
        }
      })
    }
  })
}

generate()
